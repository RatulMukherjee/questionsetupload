 <?php

require_once("dbconfig.php");
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 0;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

if(isset($_POST["submit"])) {
    $uploadOk = 1;
    
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "txt"  ) {
    echo "Sorry, only text files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        
        $directory = "uploads/".$_FILES["fileToUpload"]["name"];
//echo $directory;

$myfile = fopen($directory, "r") or die("Unable to open file!");

$arr= array();
while(!feof($myfile)) {
  if (fgetc($myfile) =="*"){
        
        array_push($arr,fgets( $myfile));
       
  }
}
fseek($myfile, 0);
$options = array();
while(!feof($myfile)) 
{
  if ( fgetc($myfile)== "#" )
  {
     
      if(fgetc($myfile)== "$")  
      {
            
         
          array_push($options,fgets( $myfile));
          
      }
      else
      {
         
          fseek($myfile, -1 , SEEK_CUR );
          
         array_push($options,fgets( $myfile));
          
      }
 }   
  }


$answers = array();
fseek($myfile, 0);
while(!feof($myfile)) {
  if (fgetc($myfile) =="$"){
     
      array_push($answers,fgets( $myfile));
  }
}


$data =array();


static $j=0;
  for($i=0; $i<count($arr); $i++ )
  {

      array_push($data,$arr[$i]);
      $m=0;
      while($m<4)
      {
          array_push($data,$options[$j]);
          $j++;$m++;
      }
      array_push($data,$answers[$i]); 
  }

fclose($myfile);

 static $k=0;

  while($k<count($data))
  {
    $question = $data[$k++];
    $option1  = $data[$k++];
    $option2  = $data[$k++];
    $option3  = $data[$k++];
    $option4  = $data[$k++];
    $answer   = $data[$k++]; 
        
     $sql = "INSERT INTO questions (question,option1,option2,option3,option4,answer)
        VALUES ('".$question."','".$option1."','".$option2."','".$option3."','".$option4."','".$answer."')";

     $conn->query($sql);
        
     
  }
 $conn->close();
        
        
    } 
    else 
    {
        echo "Sorry, there was an error uploading your file.";
    }
}


?> 